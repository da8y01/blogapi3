﻿using AutoMapper;
using DDDInfrastructure;
using DomainApplication.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BlogApi3
{
    public class PostUseCase : IPostUseCase
    {
        private readonly IMapper _mapper;

        public PostUseCase()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<GenericProfile>()));
        }

        public ResponseGeneric<ICollection<PostDTO>> ObtenerPublicaciones()
        {
            var response = new ResponseGeneric<ICollection<PostDTO>>();

            try
            {
                using (var repository = new Repository())
                {
                    Service service = new Service(repository);
                    ICollection<PostDTO> posts = service.ObtenerPosts();
                    response.Data = _mapper.Map<ICollection<PostDTO>>(posts);
                }
                response.Succeeded = true;
            }
            catch (Exception e)
            {
                response.Message = e.ToString();
            }

            return response;
        }

        public ResponseGeneric<PostDTO> ObtenerPostPorId(int id)
        {
            var response = new ResponseGeneric<PostDTO>();

            try
            {
                using (var repository = new Repository())
                {

                    response.Data = _mapper.Map<PostDTO>(new Service(repository).ObtenerPostPorId(id));
                }
                response.Succeeded = true;
            }
            catch (Exception e)
            {
                response.Message = e.ToString();
            }

            return response;
        }

        public ResponseGeneric<PostDTO> CrearPublicacion(PostDTO postDTO)
        {
            var response = new ResponseGeneric<PostDTO>();

            try
            {
                
                using (var repository = new Repository())
                {
                    response.Data = _mapper.Map<PostDTO>(new Service(repository).CrearPost(postDTO));
                }
                response.Succeeded = true;
            }
            catch (Exception e)
            {
                response.Message = e.ToString();
            }

            return response;
        }

        public ResponseGeneric<PostDTO>? ActualizarPublicacion(int id, PostDTO postDTO)
        {
            var response = new ResponseGeneric<PostDTO>();

            try
            {
                using (var repository = new Repository())
                {
                    Service service = new Service(repository);
                    PostDTO action = service.ActualizarPost(id, postDTO).Value;
                    response.Data = _mapper.Map<PostDTO>(action);
                }
                response.Succeeded = true;
            }
            catch (Exception e)
            {
                response.Message = e.ToString();
            }

            return response;
        }

        public ResponseGeneric<int>? EliminarPublicacion(int id)
        {
            var response = new ResponseGeneric<int>();

            try
            {

                using (var repository = new Repository())
                {
                    Service service = new Service(repository);
                    ActionResult<int> action = service.EliminarPost(id);
                    if (action == null) return null;
                    var update = action.Value;
                    response.Data = _mapper.Map<int>(update);
                }
                response.Succeeded = true;
            }
            catch (Exception e)
            {
                response.Message = e.ToString();
            }

            return response;
        }
    }
}