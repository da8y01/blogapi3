﻿using DDDInfrastructure;
using DDDInfrastructure.Contracts;
using DomainApplication.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BlogApi3
{
    public class Service
    {
        private IRepository _repository;

        public Service(IRepository repository)
        {
            _repository = repository;
        }


        public ICollection<PostDTO> ObtenerPosts()
        {
            return _repository.GetPosts().Select(x => PostToDTO(x)).ToList();
        }

        public PostDTO ObtenerPostPorId(int id)
        {
            return PostToDTO(_repository.GetPostById(id));
        }

        public PostDTO CrearPost(PostDTO postDTO)
        {
            Post post = new()
            {
                Content = postDTO.Content,
            };
            return PostToDTO(_repository.CreatePost(post));
        }

        public ActionResult<PostDTO> ActualizarPost(int id, PostDTO postDTO)
        {
            Post post = new()
            {
                Content = postDTO.Content,
            };
            Post updatedPost = _repository.UpdatePost(id, postDTO).Value;
            return PostToDTO(updatedPost);
        }

        public ActionResult<int> EliminarPost(int id)
        {
            return _repository.DeletePost(id);
        }

        private static PostDTO PostToDTO(Post post) =>
            new()
            {
                Id = post.Id,
                Content = post.Content
            };
    }
}
