#nullable disable
using Microsoft.AspNetCore.Mvc;
using DomainApplication.Entities;

namespace BlogApi3.Controllers
{
    [Route("api/Posts")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostUseCase _useCase;

        public PostsController()
        {
            _useCase = new PostUseCase();
        }

        //public PostsController(PostUseCase useCase)
        //{
        //    _useCase = useCase;
        //}

        // GET: api/Posts
        [HttpGet]
        public async Task<ActionResult<ResponseGeneric<IEnumerable<PostDTO>>>> GetPosts()
        {
            var result = _useCase.ObtenerPublicaciones();
            if (!result.Succeeded)
                return BadRequest(result.Message);
            return new JsonResult(result);
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ResponseGeneric<PostDTO>>> GetPost(int id)
        {
            var result = _useCase.ObtenerPostPorId(id);
            if (!result.Succeeded)
                return BadRequest(result.Message);
            return new JsonResult(result);
        }

        // PUT: api/Posts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<ResponseGeneric<PostDTO>>> PutPost(int id, PostDTO postDTO)
        {
            var result = _useCase.ActualizarPublicacion(id, postDTO);
            if (!result.Succeeded)
                return BadRequest(result.Message);
            return new JsonResult(result);
        }

        // POST: api/Posts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ResponseGeneric<PostDTO>>> PostPost(PostDTO postDTO)
        {
            var result = _useCase.CrearPublicacion(postDTO);
            if (!result.Succeeded)
                return BadRequest(result.Message);
            return new JsonResult(result);
        }

        // DELETE: api/Posts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var result = _useCase.EliminarPublicacion(id);
            if (!result.Succeeded)
                return BadRequest(result.Message);
            return new JsonResult(result);
        }
    }
}
