﻿using DomainApplication.Entities;

namespace BlogApi3
{
    public interface IPostUseCase
    {
        public ResponseGeneric<ICollection<PostDTO>> ObtenerPublicaciones();

        public ResponseGeneric<PostDTO> ObtenerPostPorId(int id);

        public ResponseGeneric<PostDTO> CrearPublicacion(PostDTO postDTO);

        public ResponseGeneric<PostDTO> ActualizarPublicacion(int id, PostDTO postDTO);

        public ResponseGeneric<int> EliminarPublicacion(int id);
    }
}